var DonationHeaders   = [ "supporter", "Date", "Description", "Amount" ];
var DonationColumns   = [ "supporter_id", "date", "desc", "amount" ];
  
var DonorColumnTitles     = [ "Donor Number", "Name", "Primary Address" ];
var DonorColumnNames      = [ "donor_id", "name", "address[primary='true']" ];

var app = angular.module('DonorApplication', []);