GridNode = function()
{
  this.gridNodeColumns    = [];
  this.gridNodeData       = [];
  this.internalID         = 0;
};
//------------------------------------------------------------------------------
GridNode.prototype.loadFromXML = function( xmlNode, gridColumns )
{
  this.internalID         = jQuery( xmlNode ).attr('id');
  for( i = 0; i < gridColumns.length; i++ )
  {
      this.gridNodeColumns.push( gridColumns[ i ] );  
  }
  
  for( i = 0; i < this.gridNodeColumns.length; i++ )
  {
      this.gridNodeData.push( getNodeValue( xmlNode, this.gridNodeColumns[ i ] ) );
  }    
};
//------------------------------------------------------------------------------
GridNode.prototype.copy = function( gridNodeParam )
{
    for( var j = 0; j < gridNodeParam.gridNodeColumns.length; j++ )
    {
        this.gridNodeColumns.push( gridNodeParam.gridNodeColumns[ j ] );  
    }
  
    for( var k = 0; k < gridNodeParam.gridNodeColumns.length; k++ )
    {
        this.gridNodeData.push( gridNodeParam.gridNodeData[ k ] );
    }
};
//------------------------------------------------------------------------------
GridNode.prototype.getFieldValue = function( fieldName )
{
    var Result = "";
    for( i = 0; i < this.gridNodeColumns.length; i++ )
    {
        if( fieldName === this.gridNodeColumns[ i ] )
        {
            Result = this.gridNodeData[i];
            break;
        }
    }
    return Result;
};
//------------------------------------------------------------------------------