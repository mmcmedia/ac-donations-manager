jQuery( document ).on( "db_selected", { foo: "bar" }, function( event, arg1 ) 
{
      jQuery( "#before_db"      ).hide();
      jQuery( "#after_db"       ).show();
      jQuery( "#new_donor_form" ).hide();
      jQuery( "#donation_tab"   ).hide();
      jQuery( "#new_donation_form" ).hide();
});

jQuery( document ).on( "show_donorform", { foo: "bar" }, function( event, arg1 ) 
{
    if( arg1 !== null )
    {
        editdonorid       = jQuery( arg1 ).attr('id');
        var donorid       = getNodeValue( arg1, "donor_id" );
        var donorname     = getNodeValue( arg1, "name" );
        var donorrelat    = getNodeValue( arg1, "relationship" );
    
        var addressnode   = jQuery( arg1 ).find( "address[primary='true']" );
    
        var donoraddress  = getNodeValue( addressnode, "address1" );
        var donoraddress2 = getNodeValue( addressnode, "address2" );
        var donorcity     = getNodeValue( addressnode, "city" );
        var donorstate    = getNodeValue( addressnode, "state" );
        var donorzip      = getNodeValue( addressnode, "zip" );
        var country       = getNodeValue( addressnode, "country" );
        var email         = getNodeValue( arg1, "email[primary='true']" );
        var homenum       = getNodeValue( arg1, "number[type='home']" );
        var cellnum       = getNodeValue( arg1, "number[type='cell']" );
  
        elementDecodeVal( "#donor-id",            donorid       );
        elementDecodeVal( "#donor-name",          donorname     );
        elementDecodeVal( "#donor-relationship",  donorrelat    );
        elementDecodeVal( "#donor-address",       donoraddress  );
        elementDecodeVal( "#donor-address2",      donoraddress2 );
        elementDecodeVal( "#donor-city",          donorcity     );
        elementDecodeVal( "#donor-state",         donorstate    ); 
        elementDecodeVal( "#donor-zip",           donorzip      );
        elementDecodeVal( "#donor-country",       country       );
        elementDecodeVal( "#donor-email",         email         );
        elementDecodeVal( "#donor-homephone",     homenum       );
        elementDecodeVal( "#donor-cellphone",     cellnum       );
    }
    else
    {
        jQuery( "#donor-id"           ).val( "" );
        jQuery( "#donor-name"         ).val( "" );
        jQuery( "#donor-relationship" ).val( "" );
        jQuery( "#donor-address"      ).val( "" );
        jQuery( "#donor-address2"     ).val( "" );
        jQuery( "#donor-city"         ).val( "" );
        jQuery( "#donor-state"        ).val( "" );
        jQuery( "#donor-zip"          ).val( "" );
        jQuery( "#donor-country"      ).val( "" );
        jQuery( "#donor-email"        ).val( "" );
        jQuery( "#donor-homephone"    ).val( "" );
        jQuery( "#donor-cellphone"    ).val( "" );
    }
    jQuery( "#new_donor_form" ).show();
    jQuery( "#donor_tab"      ).hide();
});

jQuery( document ).on( "show_import", { foo: "bar" }, function( event, arg1 ) 
{
    jQuery( "#donor_tab"      ).hide();
    jQuery( "#import_div"     ).show();
});


jQuery( document ).on( "close_import", { foo: "bar" }, function( event, arg1 ) 
{
    jQuery( "#donor_tab"      ).show();
    jQuery( "#import_div"     ).hide();
});

jQuery( document ).on( "close_donor", { foo: "bar" }, function( event, arg1 ) 
{
    jQuery( "#new_donor_form" ).hide();
    jQuery( "#donor_tab"      ).show();
});

jQuery( document ).on( "show_donor_tab", { foo: "bar" }, function( event, arg1 ) 
{
    jQuery( "#donation_tab"   ).hide();
    jQuery( "#donor_tab"      ).show();
});

jQuery( document ).on( "show_donation_tab", { foo: "bar" }, function( event, arg1 ) 
{
    jQuery( "#donation_tab"   ).show();
    jQuery( "#donor_tab"      ).hide();
});

jQuery( document ).on( "show_donation_form", { foo: "bar" }, function( event, arg1 ) 
{
    jQuery( "#new_donation_form" ).show();
    jQuery( "#donation_tab" ).hide();
    if( arg1 !== null )
    {
        var donorid         = getNodeValue( arg1, "supporter_id"  );
        var donationDate    = translateDBDate( getNodeValue( arg1, "date" ) );
        var donationAmount  = getNodeValue( arg1, "amount"        );
        var donationDesc    = getNodeValue( arg1, "desc"          );
        
        elementDecodeVal( "#donation-donor-choice", donorid         );
        elementDecodeVal( "#donation-date",         donationDate    );
        elementDecodeVal( "#donation-amount",       donationAmount  );
        elementDecodeVal( "#donation-description",  donationDesc    );
    }
    else
    {
        jQuery( "#donation-donor-choice"  ).val( "" );
        jQuery( "#donation-date"          ).val( "" );
        jQuery( "#donation-amount"        ).val( "" );
        jQuery( "#donation-description"   ).val( "" );
    }
});


jQuery( document ).on( "close_donation_form", { foo: "bar" }, function( event, arg1 ) 
{
    jQuery( "#new_donation_form" ).hide();
    jQuery( "#donation_tab" ).show();
});
