app.factory( "DatabaseService", ['$q', '$timeout',
 function( $q, $timeout ) 
 {
    var databasefile = {};
    databasefile.chosenEntry    = null;
    databasefile.textdbfile     = "";
    databasefile.databaseXML    = "";
    databasefile.donorArray     = [];
    databasefile.donationArray  = [];
    
    databasefile.restoreLastFile = function()
    {
        return $q( function( resolve, reject ) 
        {
        chrome.storage.sync.get( "db_file", function( obj )
        {
            if( obj !== null )
            {
                var entryID = obj[ "db_file" ];
                if( entryID.length > 0 )
                {
                    chrome.fileSystem.isRestorable( entryID, function( isRestorable )
                    {
                        if( isRestorable )
                        {
                            chrome.fileSystem.restoreEntry( entryID, function( theEntry )
                            {
                                databasefile.chosenEntry    = theEntry;
                                databasefile.textdbfile     = theEntry.fullPath;
                                databasefile.loadFileEntry(theEntry, resolve, reject ); 
                            });
                        }
                        else
                        {
                            resolve( databasefile.donorArray );
                        }
                    });
                }
                else
                {
                    resolve( databasefile.donorArray );
                }
            }
        });
        }, 1000 );
    };
//------------------------------------------------------------------------------
    databasefile.loadNewFile = function()
    {
        return $q( function( resolve, reject ) 
        {
        var newFileName = jQuery( "#new_file_name" ).val();
        if( ( newFileName !== null ) && ( newFileName.length > 0 ) )
        {
            var txtPos = newFileName.search( ".txt" );
            if( txtPos <= 0 )
            {
                newFileName += ".txt";
            }
        }
        chrome.fileSystem.chooseEntry( {
            type: 'saveFile',
            suggestedName: newFileName,
            accepts: [ { description: 'Text files (*.txt)',
                   extensions: ['txt']} ],
            acceptsAllTypes: true
        }, function( theEntry )
        {
            if( !theEntry )
            {
                console.log( 'No file selected.' );
                resolve( databasefile.donorArray );
                return;
            }
            databasefile.chosenEntry = theEntry;
            databasefile.textdbfile = theEntry.fullPath;
            databasefile.loadFileEntry(theEntry, resolve, reject ); 
        });
        }, 1000 );
    };
//------------------------------------------------------------------------------
    databasefile.loadDataFile = function()
    {
        return $q( function( resolve, reject ) 
        {
        //extensions: [ 'js', 'css', 'txt', 'html', 'xml', 'tsv', 'csv', 'rtf']
        var accepts = [{
            mimeTypes: ['text/*'],
            extensions: [ 'txt' ]
        }];   
        chrome.fileSystem.chooseEntry({type: 'openFile', accepts: accepts}, function(theEntry) 
        {
            if (!theEntry) 
            {
                console.log( 'No file selected.' );
                return;
            }
            databasefile.chosenEntry = theEntry;
            // use local storage to retain access to this file
            //chrome.storage.local.set({'chosenFile': chrome.fileSystem.retainEntry(theEntry)});
            databasefile.textdbfile = theEntry.fullPath;
            databasefile.loadFileEntry(theEntry);
        });
        }, 1000 );
    };
//------------------------------------------------------------------------------
    databasefile.loadFileEntry = function(entry, resolve, reject ) 
    {
        // use local storage to retain access to this file
        var retainedEntry = chrome.fileSystem.retainEntry(entry);
        chrome.storage.local.set({'chosenFile': retainedEntry});
  
        //log('Opening: ' + entry.fullPath);
        chrome.storage.sync.set( { 'db_file': retainedEntry }, function()
        {
            // do nothing now
        });
        entry.file(function(file) 
        {
            var reader = new FileReader();
            reader.readAsText(file, "utf-8");
            reader.onload = function(ev) 
            {
                databasefile.readFileContents(ev.target.result);
                resolve( databasefile.donorArray );
            };
        }, error);
    };
//------------------------------------------------------------------------------
    databasefile.readFileContents = function( fileContents )
    { 
        if( fileContents && ( fileContents.length > 0 ) )
        {
            databasefile.databaseXML = fileContents;
        }
        else
        {
            var startingXML   = "<app_data><supporters></supporters><donations></donations><expenses></expenses></app_data>";
            databasefile.databaseXML   = startingXML;
            databasefile.save( startingXML );
        }
        databasefile.loadGridArrays();
        jQuery( document ).trigger( "db_selected", [ databasefile.databaseXML ] );
  };
//------------------------------------------------------------------------------  
    databasefile.save = function() 
    {
        if( databasefile.chosenEntry !== null )
        {
            databasefile.writeFileEntry( databasefile.chosenEntry );
        }
    };  
//------------------------------------------------------------------------------
    databasefile.writeFileEntry = function (writableEntry ) 
    {
        if (!writableEntry) 
        {
            log( 'Nothing selected.' );
            return;
        }
      
        var opt_blob = new Blob([databasefile.databaseXML], {type: 'text/plain'});
        var opt_blobsize = databasefile.databaseXML.length;
        writableEntry.createWriter(function(fileWriter) 
        {
            var truncated = false;
            fileWriter.onwriteend = function() 
            {
                if (!truncated) 
                {
                    truncated = true;
                    this.truncate( opt_blobsize );
                    return;
                }
            };
            fileWriter.write( opt_blob );
        });
    };
//------------------------------------------------------------------------------
    databasefile.loadGridArrays = function()
    {
        databasefile.loadDonorGridArray();
    };
//------------------------------------------------------------------------------
    databasefile.loadDonorGridArray = function()
    {
        var xmltext = jQuery.parseXML( databasefile.databaseXML );
  	    var currRow = 0;
  	    var tempdonorArray = [];
        databasefile.donorArray.length = 0;
        databasefile.donationArray.length = 0;

	      jQuery( xmltext ).find( "supporter" ).each( function(index)
	      { 
	          var newItem = new GridNode();
	          newItem.loadFromXML( this, DonorColumnNames );
	          databasefile.donorArray.push( newItem );
	      });
	      
	      jQuery( xmltext ).find( "donation" ).each( function(index)
	      {
	        	var newItem = new GridNode();
	          newItem.loadFromXML( this, DonationColumns );
	          databasefile.donationArray.push( newItem );
	      });
    };
//------------------------------------------------------------------------------
    return databasefile;
}]);