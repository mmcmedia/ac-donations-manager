app.controller('myCtrl', ['$scope', '$q', 'DatabaseService', 'ImportService', function($scope, $q, DatabaseService, ImportService )  
{
    //$scope.dataContent    = new DataContent( $scope );
    $scope.isEdit         = false;
    $scope.editID         = 0;
    $scope.databasefile   = DatabaseService;
    $scope.importclass    = ImportService;
    $scope.currentIndex   = 0;
    
    $scope.donors         = DatabaseService.donorArray;
    $scope.donations      = DatabaseService.donationArray;
    $scope.donationsFiltered = [];
    
    $scope.dateComboChanging = false;
    $scope.currentDateChoice = "all_dates";
    $scope.currentDateOne    = "";
    $scope.currentDateTtwo   = "";
    $scope.supportFilter     = "all_donors";
    
    $scope.chartLabelList = [];
	  $scope.chartDataList = [];
    
    var restorePromise = DatabaseService.restoreLastFile();
    restorePromise.then( function( returned ) {
      $scope.donors = returned; 
      //$scope.donations = DatabaseService.donationArray;
      $scope.donationsFiltered = $scope.getFilteredDonations();
      //var donationPromise = $scope.searchDonationResolve();
      //donationPromise.then( function( filterResult ) {
        //$scope.donationsFiltered = filterResult;
      //});
    });
      
    $scope.changeFile = function() 
    {
        $scope.databasefile.loadDataFile();
    };
//------------------------------------------------------------------------------
    $scope.loadNewFile = function()
    {
       $scope.databasefile.loadNewFile();
    };
//------------------------------------------------------------------------------
    $scope.loadFile = function()
    {
       $scope.databasefile.loadDataFile();
    };
//------------------------------------------------------------------------------
    $scope.addDonor = function()
    {
        $scope.isEdit = false;
        $scope.editID = 0;
        jQuery( document ).trigger( "show_donorform", [ null ] );
    };
//------------------------------------------------------------------------------
    $scope.editDonor = function()
    {
        var editDonorID = $scope.getDonorRowID();
        $scope.editID   = parseInt( editDonorID ); 
        
        var xmltext       = jQuery.parseXML( $scope.databasefile.databaseXML );
        var DonorChecked  = jQuery( xmltext ).find( "supporter[id='" + editDonorID + "']" );
        $scope.isEdit     = true;
        
        jQuery( document ).trigger( "show_donorform", [ DonorChecked ] );
    };
//------------------------------------------------------------------------------
    $scope.removeDonors = function()
    { 
        var removeDonorArray  = $scope.getDonorsSelected();
        var newDataXML        = removeDonorXML( $scope.databasefile.databaseXML, removeDonorArray );
        $scope.UpdateXML( newDataXML );
    };
//------------------------------------------------------------------------------
    $scope.import = function()
    {
        jQuery( document ).trigger( "show_import", [ "" ] ); 
    };
//------------------------------------------------------------------------------
    $scope.cancelDonor = function()
    {
        $scope.isEdit = false;
        $scope.editID = 0;
        jQuery( document ).trigger( "close_donor", [ "" ] ); 
    };
//------------------------------------------------------------------------------
    $scope.saveDonor = function()
    {
        if( $scope.isEdit === true )
        {
            var updatedXML = updateNewDonorXML( $scope.databasefile.databaseXML, $scope.editID );
            $scope.UpdateXML( updatedXML );
        }
        else
        {
            var nextID      = $scope.getNextDonorID();
            var donorXML    = getDonorXML( nextID ); 
            var newDataXML  = addNewDonorXML( $scope.databasefile.databaseXML, donorXML );
            $scope.UpdateXML( newDataXML );
        }
        $scope.isEdit = false;
        $scope.editID = 0;
        jQuery( document ).trigger( "close_donor", [ "" ] ); 
    };
//------------------------------------------------------------------------------
    $scope.saveDonation = function()
    {
        if( $scope.isEdit === true )
        {
            var updatedXML = updateNewDonationXML( $scope.databasefile.databaseXML, $scope.editID );
            $scope.UpdateXML( updatedXML );
        }
        else
        {
            var nextID      = $scope.getNextDonationID();
            var donationXML = getDonationXML( nextID ); 
            var newDataXML  = addNewDonorXML( $scope.databasefile.databaseXML, donationXML );
            $scope.UpdateXML( newDataXML );
        }
        $scope.isEdit = false;
        $scope.editID = 0;
        jQuery( document ).trigger( "close_donation_form", [ "" ] ); 
    };
//------------------------------------------------------------------------------
    $scope.cancelDonation = function()
    {
        jQuery( document ).trigger( "close_donation_form", [ "" ] ); 
    };
//------------------------------------------------------------------------------
    $scope.lookupDonorName = function( $donor_id )
    {
        var Result = "";
        for( i =0; i < $scope.donors.length; i++ )
        {
            var donorObj = $scope.donors[ i ];
            if( donorObj.internalID === $donor_id )
            {
                Result = donorObj.getFieldValue( "name" );
                break;
            }
        }
        return Result;
    };
//------------------------------------------------------------------------------
    $scope.getDonorRowID = function()
    {
        var Result = "0";
        for( i =0; i < $scope.donors.length; i++ )
        {
            var rowIndex = parseInt( i ) + 1;
            var checkboxid = "#donatebox_" + rowIndex.toString();
            if( jQuery( checkboxid ).is(':checked') )
            {
                Result = $scope.donors[ i ].internalID;
                break;
            }
        }
        return Result;
    };
//------------------------------------------------------------------------------
    $scope.getDonationRowID = function()
    {
        var Result = "0";
        for( i =0; i < $scope.donations.length; i++ )
        {
            var rowIndex = parseInt( i ) + 1;
            var checkboxid = "#donationbox_" + rowIndex.toString();
            if( jQuery( checkboxid ).is(':checked') )
            {
                Result = $scope.donations[ i ].internalID;
                break;
            }
        }
        return Result;
    };
//------------------------------------------------------------------------------
    $scope.getDonorsSelected = function()
    {
        var Result = [];
        for( i = 0; i < $scope.donors.length; i++ )
        {
            var rowIndex = parseInt( i ) + 1;
            var checkboxid = "#donatebox_" + rowIndex.toString();
            if( jQuery( checkboxid ).is(':checked') )
            {
                Result.push( $scope.donors[ i ].internalID );
            }
        }
        return Result;
    };
//------------------------------------------------------------------------------
    $scope.getDonationsSelected = function()
    {
        var Result = [];
        for( i = 0; i < $scope.donations.length; i++ )
        {
            var rowIndex = parseInt( i ) + 1;
            var checkboxid = "#donationbox_" + rowIndex.toString();
            if( jQuery( checkboxid ).is(':checked') )
            {
                Result.push( $scope.donations[ i ].internalID );
            }
        }
        return Result;
    };
//------------------------------------------------------------------------------
    $scope.getNextDonorID = function()
    {
        var Result = 0;
        for( i =0; i < $scope.donors.length; i++ )
        {
            var donorItem   = $scope.donors[ i ];
            var tempResult  = parseInt( donorItem.internalID );
            if( tempResult > Result )
            {
                Result = tempResult;
            }
        }
        return Result;
    };
//------------------------------------------------------------------------------
    $scope.getNextDonationID = function()
    {
        var Result = 0;
        for( i =0; i < $scope.donations.length; i++ )
        {
            var donorItem   = $scope.donations[ i ];
            var tempResult  = parseInt( donorItem.internalID );
            if( tempResult > Result )
            {
                Result = tempResult;
            }
        }
        return Result;
    };
//------------------------------------------------------------------------------
    $scope.UpdateXML = function( updatedXMLText )
    {
        $scope.databasefile.databaseXML = updatedXMLText;
      
        $scope.databasefile.save();
    
        $scope.databasefile.loadGridArrays();
    };
//------------------------------------------------------------------------------
    $scope.refreshGrid = function()
    {
    };
//------------------------------------------------------------------------------
    $scope.importText = function()
    {
        var updatedText = $scope.importclass.importText( $scope.databasefile.databaseXML );
        $scope.UpdateXML( updatedText );
        jQuery( document ).trigger( "close_import", null );
    };
//------------------------------------------------------------------------------
    $scope.importCancel = function()
    {
        jQuery( document ).trigger( "close_import", null );
    };
//------------------------------------------------------------------------------
    $scope.selectDonorTab = function()
    {
        $scope.donors         = $scope.databasefile.donorArray;
        $scope.donations      = $scope.databasefile.donationArray;
        jQuery( "#donation_nav_tab"  ).removeClass( "current" );
        jQuery( "#donor_nav_tab"     ).addClass( "current" );
        $scope.currentIndex   = 0;
        jQuery( document ).trigger( "show_donor_tab", null );
    };
//------------------------------------------------------------------------------
    $scope.selectDonationTab = function()
    {
        $scope.donors         = $scope.databasefile.donorArray;
        $scope.donations      = $scope.databasefile.donationArray;
        jQuery( "#donation_nav_tab"  ).addClass( "current" );
        jQuery( "#donor_nav_tab"     ).removeClass( "current" );
        $scope.currentIndex   = 1;
        jQuery( document ).trigger( "show_donation_tab", null );
    };
//------------------------------------------------------------------------------
    $scope.addDonation = function()
    {
        $scope.isEdit = false;
        $scope.editID = 0;
        jQuery( document ).trigger( "show_donation_form", [ null ] );
    };
//------------------------------------------------------------------------------
    $scope.editDonation = function()
    {
        var editDonationID = $scope.getDonationRowID();
        $scope.editID   = parseInt( editDonationID ); 
        
        var xmltext           = jQuery.parseXML( $scope.databasefile.databaseXML );
        var DonationChecked   = jQuery( xmltext ).find( "donation[id='" + editDonationID + "']" );
        $scope.isEdit         = true;
        
        jQuery( document ).trigger( "show_donation_form", [ DonationChecked ] );
    };
//------------------------------------------------------------------------------
    $scope.removeDonations = function()
    { 
        var removeDonorArray  = $scope.getDonationsSelected();
        var newDataXML        = removeDonorXML( $scope.databasefile.databaseXML, removeDonorArray );
        $scope.UpdateXML( newDataXML );
    };
//------------------------------------------------------------------------------
    $scope.donationDateFilter = function()
    {
        $scope.dateComboChanging = true;
        var dateChoice = jQuery( "#date_range_choice" ).val();
        var todayDate = new Date();
        if( dateChoice === "all_dates" )
        {
            $scope.currentDateOne = "";
            $scope.currentDateTwo = "";
        }
        else if( dateChoice === "this_week" )
        {
            $scope.currentDateOne = getDateDifference( todayDate, -7, 0, 0 );
            $scope.currentDateTwo = todayDate;
        }
        else if( dateChoice === "this_month" )
        {
            $scope.currentDateOne = getDateDifference( todayDate, 0, -1, 0 );
            $scope.currentDateTwo = todayDate;
        }
        else if( dateChoice === "this_year" )
        {
            $scope.currentDateOne = getDateDifference( todayDate, 0, 0, -1 );
            $scope.currentDateTwo = todayDate;
        }
        $scope.currentDateChoice = dateChoice; 
        $scope.dateComboChanging = false;
    };
//------------------------------------------------------------------------------
    $scope.dateInputOneChange = function()
    { 
        if( $scope.dateComboChanging === false )
        {
            $scope.currentDateChoice  = "custom";
            $scope.currentDateOne     = getInputDateValue( jQuery( "#date_range_one" ).val() );  
        }
    };  
//------------------------------------------------------------------------------
    $scope.dateInputTwoChange = function()
    { 
        if( $scope.dateComboChanging === false )
        {
            $scope.currentDateChoice  = "custom";
            $scope.currentDateTwo     = getInputDateValue( jQuery( "#date_range_two" ).val() );  
        }
    }; 
//------------------------------------------------------------------------------
    $scope.onSupportFilterChange = function()
    {
        $scope.supportFilter = jQuery( "#donation-search-choice" ).val();
    };
//------------------------------------------------------------------------------
    $scope.getFilteredDonations = function()
    {
        var filteredResults = [];
        for( var i = 0; i < $scope.donations.length; i++ )
        {
            var donation = $scope.donations[ i ];
            var canAdd = true;
            if( $scope.currentDateChoice !== "all_dates" )
            {
                var donateDate = getInputDateValue( donation.getFieldValue( "date" ) );
                if( ( donateDate < $scope.currentDateOne ) || ( donateDate > $scope.currentDateTwo ) )
                {
                    canAdd = false;
                }
            }
            if( canAdd && ( $scope.supportFilter !== "all_donors" ) )
            {
                var donateSupporter = donation.getFieldValue( "supporter_id" );
                if( $scope.supportFilter !== donateSupporter )
                {
                    canAdd = false;
                }
            }
            if( canAdd )
            {
                var newItem = new GridNode();
                newItem.copy( donation );
                filteredResults.push( newItem ); 
            }
        }
        return filteredResults;
    };
//------------------------------------------------------------------------------
    $scope.searchDonations = function()
    {
        $scope.donationsFiltered = $scope.getFilteredDonations();
        $scope.loadDonationGrid();
    };
//------------------------------------------------------------------------------
    $scope.getTotalFilteredDonations = function()
    {
        var totalAmount = 0;
        for( var i = 0; i < $scope.donationsFiltered.length; i++ )
        {
            var donation = $scope.donationsFiltered[ i ];
            totalAmount += parseFloat( donation.getFieldValue( "amount" ) ); 
        }
        return totalAmount.toFixed(2);
    };
//------------------------------------------------------------------------------
    $scope.getTotalByMonth = function( monthVal, yearVal )
    {
        var totalMonth = 0;
        
        for( var i = 0; i < $scope.donationsFiltered.length; i++ )
        {
            var donation = $scope.donationsFiltered[ i ];
            var donationDate = donation.getFieldValue( "date" );
            
            var dashPos = donationDate.search( "-" );
            var donationMonth = 0;
            var donationYear  = 0;
            var currentPos    = 0;
            if( dashPos > 0 )
            {
                currentPos    = donationDate.search( "-" );
                donationYear  = donationDate.substring( 0, currentPos );
                donationDate  = donationDate.substring( currentPos + 1, donationDate.length );
                currentPos    = donationDate.search( "-" );
                donationMonth = donationDate.substring( 0, currentPos );
            }
            else
            {
                currentPos    = donationDate.search( "/" );
                donationMonth = donationDate.substring( 0, currentPos );
                donationDate  = donationDate.substring( currentPos + 1, donationDate.length );
                currentPos    = donationDate.search( "/" );
                donationYear  = donationDate.substring( currentPos + 1, donationDate.length ); 
            }
            
            if( ( donationMonth === monthVal ) && ( donationYear === yearVal ) )
            {
                totalMonth += parseFloat( donation.getFieldValue( "amount" ) ); 
            }
        }
        
        return totalMonth;
    };
//------------------------------------------------------------------------------
    $scope.loadDonationGrid = function()
    {
      	$scope.chartLabelList.length = 0;
	      $scope.chartDataList.length = 0;
        if( $scope.currentDateChoice !== "all_dates" )
        {
            getInputDateLabels( $scope.currentDateOne, $scope.currentDateTwo, $scope.chartLabelList );
            for( var i = 0; i < $scope.chartLabelList.length; i++ )
            {
                var labelVal = $scope.chartLabelList[ i ];
                var slashPos = labelVal.search( "/" );
                var monthVal = labelVal.substring( 0, slashPos );
                var yearVal  = labelVal.substring( slashPos + 1, labelVal.length );
                $scope.chartDataList.push( $scope.getTotalByMonth( monthVal, yearVal ) );
            } 
      
	         Chartist.Line('.ct-chart', {
                labels: $scope.chartLabelList,
                series: [$scope.chartDataList]
                }, {
                width: '800px',
                height: '200px'
            });
        }
    };
//------------------------------------------------------------------------------
}]);