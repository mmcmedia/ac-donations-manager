app.factory( "ImportService", ['$q', '$timeout',
  function( $q, $timeout ) 
  {
      var importClass = {};
      importClass.localDonorIDs     = [];
      importClass.localDonorAltIDs  = [];
      importClass.nextDonorID       = 1;
      importClass.nextDonationID    = 1;
      importClass.startingXML       = "";
      importClass.lastDonorID       = 0;
//------------------------------------------------------------------------------
      importClass.importText = function( dataXML )
      {
          importClass.startingXML = dataXML;
    
          importClass.loadIDArrays();
  
          var importOptionIndex = jQuery("#inputTypeCombo").prop('selectedIndex');
          if( importOptionIndex === 0 )
          {
              importClass.importEmail();
          }
          else
          {
              importClass.importCSV();
          }
          return importClass.startingXML;
      };
//------------------------------------------------------------------------------
      importClass.loadIDArrays = function()
      {
          var xmltext = jQuery.parseXML( importClass.startingXML );
          importClass.localDonorIDs.length    = 0;
          importClass.localDonorAltIDs.length = 0;
          
	        jQuery( xmltext ).find( "supporter" ).each( function(index)
	        { 
	            var currentID     = jQuery( this ).attr('id');
	            var currentIDInt  = parseInt( currentID );
	            importClass.localDonorIDs.push( jQuery( this ).attr('id') );
	            importClass.localDonorAltIDs.push( getNodeValue( this, "donor_id" ) );
	            if( currentIDInt >= importClass.nextDonorID )
	            {
	                importClass.nextDonorID = CurrentIDInt + 1;
	            }
	        });  
	        
	        jQuery( xmltext ).find( "donation" ).each( function( index )
	        {
	            var currentDonationID = parseInt( jQuery( this ).attr( 'id' ) );
	            
	            if( currentDonationID >= importClass.nextDonationID )
	            {
	                importClass.nextDonationID = currentDonationID + 1;
	            }
	        });
      };
//------------------------------------------------------------------------------
      importClass.pushLocalSupporterID = function( donor_id, donor_alt_id )
      {
          importClass.localDonorIDs.push( donor_id );
          importClass.localDonorAltIDs.push( donor_alt_id );
      };
//------------------------------------------------------------------------------
      importClass.LookupLocalSupporterID = function ( donor_id )
      {
          var internalid = "";
          for( i = 0; i < importClass.localDonorAltIDs.length; i++ )
          {
              if( importClass.localDonorAltIDs[ i ] == donor_id )
              {
                  internalid = importClass.localDonorIDs[ i ];
                  break;
              }
          }
          return internalid;
      };
//------------------------------------------------------------------------------
      importClass.checkNextCSVLine = function( totalTextImport )
      {
          var nextLineIsValid = false;
    
          while( nextLineIsValid === false )
          {
              var tempTextImportPos = totalTextImport.search( "\n" );
              var tempTextImport    = totalTextImport.substring( tempTextImportPos + 1, totalTextImport.length );
    
              var nextCSVLinePos  = tempTextImport.search( "\n" );
              var nextCSVLine     = tempTextImport.substring( 0, nextCSVLinePos );
    
              var nextCSVLineComma = nextCSVLine.search( "," );
              if( nextCSVLineComma > 0 )
              {
                  var nextCSVLineValue = nextCSVLine.substring( 0, nextCSVLineComma );
                  nextCSVLine = nextCSVLine.substring( nextCSVLineComma + 1, nextCSVLine.length );
                  if( ( nextCSVLineValue !== null ) && ( nextCSVLineValue.length > 0 ) )
                  {
                      nextLineIsValid = true;
                  }
              }
    
              if( nextLineIsValid === false )
              {
                  var nextValidPos = tempTextImport.search( "DonorID" );
                  if( nextValidPos > 0 )
                  {
                      tempTextImport   = tempTextImport.substring( nextValidPos, tempTextImport.length );
        
                      totalTextImport  = tempTextImport;
                  }
                  else
                  {
                      totalTextImport = "";
                      break;
                  }
              }
          }
          return totalTextImport;
      };
//------------------------------------------------------------------------------
      importClass.importEmail = function( dataXML )
      {
          var startingText      = "Family Donor__________________";
          var startingTextTwo   = "Individual Donor______________";
          var startingTextThree = "Organization Donor____________";
          var endingText        = "__________";
  
          var textToImport  = jQuery( "#importtextbox" ).val();
          var importFamily  = textToImport.search( startingText );
          var importSingle  = textToImport.search( startingTextTwo );
          var importOrg     = textToImport.search( startingTextThree );
  
          if( importFamily > 0 )
          {
              var familydonors  = textToImport.substring( importFamily + startingText.length, importSingle );
              familydonors      = importClass.findNextLine( familydonors );
              familydonors      = importClass.findNextNonEmptyLine( familydonors );
  
              while( familydonors.length > 10 )
              {
                  familydonors = importClass.extractDonorInformation( familydonors ).trim(); 
              }
          }
          if( importSingle > 0 )
          {
              var singledonors  = textToImport.substring( importSingle + startingTextTwo.length, importOrg );
              singledonors      = importClass.findNextLine( singledonors );
              singledonors      = importClass.findNextNonEmptyLine( singledonors ); 
              while( singledonors.length > 10 )
              {
                  singledonors = importClass.extractDonorInformation( singledonors ).trim(); 
              }
          }
          if( importOrg > 0 )
          {
              var orgdonors   = textToImport.substring( importOrg + startingTextThree.length, textToImport.length );
              orgdonors       = importClass.findNextLine( orgdonors );
              orgdonors       = importClass.findNextNonEmptyLine( orgdonors );
              var endofdonors = orgdonors.search( endingText );
              orgDonors       = orgdonors.substring( 0, endingText );
              while( orgdonors.length > 10 )
              {
                  orgdonors = importClass.extractDonorInformation( orgdonors ).trim();
              }
          }
      };
//------------------------------------------------------------------------------
      importClass.findNextLine = function( completeText )
      {
          var NextEmpty = completeText.search( "\n" );
          completeText  = completeText.substring( NextEmpty + 1, completeText.length );
          return completeText;
      };
//------------------------------------------------------------------------------
      importClass.nextLineIsEmpty = function( completeText )
      {
          var nextLinePos   = completeText.search( "\n" );
          var nextLineVal   = completeText.substring( 0, nextLinePos ).trim();
          return ( nextLineVal === "" );
      };
//------------------------------------------------------------------------------
      importClass.findNextNonEmptyLine = function( completeText )
      {
          var nextCharacter = completeText.substring( 0, 1 );
          var tooManyTimes  = 0;
          while( nextCharacter === "\n" )
          {
              if( completeText.length == 1 )
              {
                  break;
              }
              if( tooManyTimes > 10 )
              {
                  completeText = "";
                  break;
              }
              completeText  = completeText.substring( 1, completeText.length );
              nextCharacter = completeText.substring( 0, 1 );
              tooManyTimes++;
          }
          return completeText;
      };
//------------------------------------------------------------------------------
      importClass.extractDonorInformation = function( completeText )
      {
          var isDoubleLine = false;
          
          // First Line id, name, phone
          var lineEnd   = completeText.search( "\n" );
          var firstLine = completeText.substring( 0, lineEnd ).trim();
          completeText  = completeText.substring( lineEnd + 1, completeText.length ); 
          //console.log( firstLine );
        
          var donorend  = firstLine.search( " " );
          var donorid   = firstLine.substring( 0, donorend ).trim();
          firstLine     = firstLine.substring( donorend, firstLine.length ).trim();
          
          var nameEnd   = firstLine.search( /\(/ );
          var nameval   = "";
          var phoneval  = "";
          if( nameEnd > 0 )
          {
              nameval     = firstLine.substring( 0, nameEnd ).trim();
              firstLine   = firstLine.substring( nameEnd, firstLine.length ).trim();
              
              phoneval  = firstLine;
          }
          else
          {
            nameval = firstLine.trim();
          }
          if( this.nextLineIsEmpty( completeText ) )
          {
              completeText  = this.findNextLine( completeText ); 
              isDoubleLine  = true;
          }
    
          // Second Line Address
          lineEnd         = completeText.search( "\n" );
          var secondLine  = completeText.substring( 0, lineEnd ).trim();
          completeText    = completeText.substring( lineEnd + 1, completeText.length ); 
          //console.log( secondLine );
          
          var addressval  = secondLine.trim();
          
          if( isDoubleLine )
          {
              completeText  = this.findNextLine( completeText );
          }
          
          lineEnd         = completeText.search( "\n" );
          var thirdLine   = completeText.substring( 0, lineEnd ).trim();
          completeText    = completeText.substring( lineEnd + 1, completeText.length ); 
          //console.log( thirdLine );
          
          var cityend     = thirdLine.search( "   " );
          var cityval     = "";
          var stateval    = "";
          var zipval      = "";
          var countryval  = "";
          var address2val = "";
          
          if( cityend > 0 )
          {
              cityval       = thirdLine.substring( 0, cityend ).trim();
              thirdLine     = thirdLine.substring( cityend, thirdLine.length ).trim();
          
              var stateend  = thirdLine.search( "   " );
              stateval      = thirdLine.substring( 0, stateend ).trim();
              thirdLine     = thirdLine.substring( stateend, thirdLine.length ).trim();
          
              var zipend    = thirdLine.search( "   " );
              zipval        = thirdLine.substring( 0, zipend ).trim();
              thirdLine     = thirdLine.substring( zipend, thirdLine.length ).trim();
              countryval    = thirdLine.trim();
          }
          else
          {
              address2val = thirdLine.trim();
          }
          
          if( isDoubleLine )
          {
              completeText  = this.findNextLine( completeText );
          }
          
          lineEnd         = completeText.search( "\n" );
          var fourthLine  = completeText.substring( 0, lineEnd ).trim();
          completeText    = completeText.substring( lineEnd + 1, completeText.length );
          //console.log( fourthLine );
          
          var phonetwoEnd  = fourthLine.search( " " );
          var phonetwoval  = "";
          var emailval     = "";
          if( phonetwoEnd > 0 )
          {
              phonetwoval = fourthLine.substring( 0, phonetwoEnd ).trim();
              fourthLine  = fourthLine.substring( phonetwoEnd, fourthLine.length ).trim();
          
              emailval  = fourthLine.trim(); 
          }
          else
          {
            if( fourthLine.search( "@" ) > 0 )
            {
              emailval = fourthLine.trim();
            }
            else
            {
              phonetwoval = fourthLine.trim();
            }
          }
          
          if( isDoubleLine )
          {
              completeText  = this.findNextLine( completeText );
          }
          
          lineEnd         = completeText.search( "\n" );
          var fifthLine   = "";
          if( lineEnd > 0 )
          {
            fifthLine     = completeText.substring( 0, lineEnd ).trim();
            completeText  = completeText.substring( lineEnd + 1, completeText.length );
          }
          else
          {
            fifthLine = completeText.trim();
            completeText = "";
          }
          //console.log( fifthLine );
    
          var donateMatches = fifthLine.match( /^[=-]*(.*)\s+(\d+\.\d{2})\s+(\d+\/\d+\/\d{4})/ );
          if( donateMatches )
          {
            var descval   = donateMatches[1].trim();
            descval = descval.replace( /=/g, '' );
            descval = descval.replace( /-/g, '' );
            var amountval   = donateMatches[2].trim();
            var dateval     = donateMatches[3].trim();
            
            importClass.importDonorRecord( donorid, nameval, addressval, address2val, cityval, stateval, zipval, countryval, emailval, phoneval, dateval, amountval, descval );
            while( importClass.nextLineIsEmpty( completeText ) === false )
            {
                var nextLinePos = completeText.search( "\n" );
                if( nextLinePos > 0 )
                {
                    var nextLineVal = completeText.substring( 0, nextLinePos );
                    completeText    = completeText.substring( nextLinePos + 1, completeText.length );
                    donateMatches   = nextLineVal.match( /^[=-]*(.*)\s+(\d+\.\d{2})\s+(\d+\/\d+\/\d{4})/ );
                    if( donateMatches )
                    {
                        descval     = donateMatches[1].trim(0);
          
                        descval     = descval.replace( /=/g, '' );
                        descval     = descval.replace( /-/g, '' );
                        amountval   = donateMatches[2].trim();
                        dateval     = donateMatches[3].trim();
                    }
                    importClass.importDonationRecord( importClass.lastDonorID, dateval, amountval, descval );
                }
                else
                {
                    break;
                }
            }
          }
          
          return completeText;
      };
//------------------------------------------------------------------------------
      importClass.importDonorRecord = function( donorID, name, address1, address2, city, state, zip, country, email, phone, donate_date, donate_amount, donate_desc )
      {
          var encodedDonorID  = InputValueEncode( donorID );
          var editdonorid     = importClass.LookupLocalSupporterID( encodedDonorID );
          var selectedID      = editdonorid;
    
          if( editdonorid > 0 )
          {
              if( jQuery( "#updateDonorCheck" ).is(':checked') )
              {
                  var parsedXML = jQuery.parseXML( importClass.startingXML );
                  var EditDonorNode = jQuery( parsedXML ).find( "supporter[id='" + editdonorid.toString() + "']" );
          
                  jQuery( EditDonorNode ).find( "donor_id"      ).text( encodedDonorID );
                  jQuery( EditDonorNode ).find( "name"          ).text( InputValueEncode( name      ) );
          
                  var addressnode   = jQuery( EditDonorNode ).find( "address[primary='true']" );
                  jQuery( addressnode ).find( "address1"  ).text( InputValueEncode( address1    ) );
                  jQuery( addressnode ).find( "address2"  ).text( InputValueEncode( address2    ) );
                  jQuery( addressnode ).find( "city"      ).text( InputValueEncode( city        ) );
                  jQuery( addressnode ).find( "state"     ).text( InputValueEncode( state       ) );
                  jQuery( addressnode ).find( "zip"       ).text( InputValueEncode( zip         ) );
                  jQuery( addressnode ).find( "country"   ).text( InputValueEncode( country     ) );
          
                  jQuery( EditDonorNode ).find( "email[primary='true']" ).text( InputValueEncode( email ) );
                  jQuery( EditDonorNode ).find( "number[type='home']"   ).text( InputValueEncode( phone ) );
          
                  var DonorRecords = jQuery( parsedXML ).find( "app_data" ).html();
                  DonorRecords = "<app_data>" + DonorRecords + "</app_data>";
                  importClass.startingXML = DonorRecords;
              }
          }
          else
          {
              if( name.trim().length > 0 )
              {
                  selectedID = importClass.nextDonorID;
                  var newDonorXml = "<supporter id='" + importClass.nextDonorID.toString() + "'>";
                  newDonorXml    += "<type></type>";
                  newDonorXml    += "<donor_id>" +  encodedDonorID + "</donor_id>";
                  newDonorXml    += "<name>" + InputValueEncode( name ) + "</name>";
                  newDonorXml    += "<relationship></relationship>";
                  newDonorXml    += "<addresses>";
                  newDonorXml    += "<address primary='true'>";
                  newDonorXml    += "<address1>" + InputValueEncode( address1 ) + "</address1>";
                  newDonorXml    += "<address2>" + InputValueEncode( address2 )  + "</address2>";
                  newDonorXml    += "<city>" + InputValueEncode( city ) + "</city>";
                  newDonorXml    += "<state>" + InputValueEncode( state )  + "</state>";
                  newDonorXml    += "<zip>" + InputValueEncode( zip ) + "</zip>";
                  newDonorXml    += "<country>" + InputValueEncode( country ) + "</country>";
                  newDonorXml    += "</address>";
                  newDonorXml    += "</addresses>";
                  newDonorXml    += "<email_addresses>";
                  newDonorXml    += "<email primary='true'>" + InputValueEncode( email ) + "</email>";
                  newDonorXml    += "</email_addresses>";
                  newDonorXml    += "<phone_numbers>";
                  newDonorXml    += "<number type='home'>" + InputValueEncode( phone ) + "</number>";
      	          newDonorXml    += "</phone_numbers>";
                  newDonorXml    += "</supporter>";
            
                  var SupporterEnd  = importClass.startingXML.search( "</supporters>" );
                  var DataEnd       = importClass.startingXML.length;
                  var StartXml      = importClass.startingXML.substring( 0, SupporterEnd );
                  var EndXml        = importClass.startingXML.substring( SupporterEnd, DataEnd );
          
                  importClass.startingXML = StartXml + newDonorXml + EndXml;
                  importClass.nextDonorID++;
              
                  importClass.pushLocalSupporterID( selectedID, encodedDonorID );
              }
          }
          importClass.lastDonorID = selectedID;
          importClass.importDonationRecord( selectedID, donate_date, donate_amount, donate_desc );
      };
//------------------------------------------------------------------------------
      importClass.importDonationRecord = function( donor_id, donate_date, donate_amount, donate_desc )
      {
          if( donor_id > 0 )
          {
              var newDonationXml = "<donation id='" + importClass.nextDonationID.toString() + "'>";
              newDonationXml    += "<supporter_id>" + donor_id.toString() + "</supporter_id>";
              newDonationXml    += "<date>" +   InputValueEncode( donate_date ) + "</date>";
              newDonationXml    += "<amount>" + InputValueEncode( donate_amount ) + "</amount>";
              newDonationXml    += "<desc>" + InputValueEncode( donate_desc ) + "</desc>";
              newDonationXml    += "</donation>";
        
              var donationAppend  = importClass.startingXML.search( "</donations>" );
              var DataLength      = importClass.startingXML.length;
              var donationStart   = importClass.startingXML.substring( 0, donationAppend );
              var donationEnd     = importClass.startingXML.substring( donationAppend, DataLength );
    
              importClass.startingXML = donationStart + newDonationXml + donationEnd;
        
              importClass.nextDonationID++;
          }
      };
//------------------------------------------------------------------------------
      importClass.importCSVGroup = function( textToImport )
      {
          var nextLinePos   = textToImport.search( "\n" );
          var headerLine    = textToImport.substring( 0, nextLinePos ); 
          textToImport      = textToImport.substring( nextLinePos + 1, textToImport.length );
    
          var endingPos     = textToImport.search( "Sub Account");
          if( endingPos > 0 )
          {
              textToImport = textToImport.substring( 0, endingPos );
          }
    
          nextLinePos     = textToImport.search( "\n" );
          while( nextLinePos > 0 )
          {
              var tempHeaderLine  = headerLine;
              var nextLineVal     = textToImport.substring( 0, nextLinePos );
              textToImport        = textToImport.substring( nextLinePos + 1, textToImport.length );
        
              var nextHeaderComma = tempHeaderLine.search( "," );
              var nextLineComma   = nextLineVal.search( "," );
              var donorid         = "";
              var nameval         = "";
              var addressval      = "";
              var address2val     = "";
              var cityval         = "";
              var stateval        = "";
              var zipval          = "";
              var countryval      = "";
              var emailval        = "";
              var phoneval        = "";
              var dateval         = "";
              var amountval       = "";
              var descval         = ""; 
        
              while( nextHeaderComma > 0 )
              {
                  var headerVal   = tempHeaderLine.substring( 0, nextHeaderComma );
                  tempHeaderLine  = tempHeaderLine.substring( nextHeaderComma + 1, tempHeaderLine.length );
            
                  var LineVal     = nextLineVal.substring( 0, nextLineComma );
                  nextLineVal     = nextLineVal.substring( nextLineComma + 1, nextLineVal.length );
                  if( LineVal.length > 0 )
                  {
                      if( headerVal === "DonorID" )
                      { 
                          donorid = LineVal;
                      }
                      else if( headerVal === "Category" )
                      {
                      }
                      else if( headerVal === "Name" )
                      {
                          nameval = LineVal;
                      }
                      else if( headerVal === "Date" )
                      {
                          dateval = LineVal;
                      }
                      else if( headerVal === "Amt" )
                      {
                          amountval = LineVal;
                      }
                      else if( headerVal === "Comments" )
                      {
                          descval = LineVal;
                      }
                      else if( headerVal === "Addr1" )
                      {
                          addressval = LineVal;
                      }
                      else if( headerVal === "Addr2" )
                      {
                          address2val = LineVal;
                      }
                      else if( headerVal === "City" )
                      {
                          cityval = LineVal;
                      }
                      else if( headerVal === "State" )
                      {
                          stateval = LineVal;
                      }
                      else if( headerVal === "Zip" )
                      {
                          zipval = LineVal;
                      }
                      else if( headerVal === "Country" )
                      {
                          countryval = LineVal;
                      }
                      else if( headerVal === "Phone" )
                      {
                          phoneval = LineVal;
                      }
                      else if( headerVal === "eMail" )
                      {
                          emailval = LineVal;
                      }
                  }
                  nextHeaderComma = tempHeaderLine.search( "," );
                  nextLineComma   = nextLineVal.search( "," );
              }
              importClass.importDonorRecord( donorid, nameval, addressval, address2val, cityval, stateval, zipval, countryval, emailval, phoneval, dateval, amountval, descval );
              nextLinePos = textToImport.search( "\n" );
          }
      };
//------------------------------------------------------------------------------
      importClass.importCSV = function()
      {
          var textToImport  = jQuery( "#importtextbox" ).val();
          var startingPos   = textToImport.search( "DonorID" );
          while( startingPos > 0 )
          {
              textToImport = textToImport.substring( startingPos, textToImport.length );
              textToImport = importClass.checkNextCSVLine( textToImport );
              if( textToImport.length > 0 )
              {
                  var endingText = "Sub Account";
                  var textToImportEnd = textToImport.search( endingText );
                  var textToImportGroup = textToImport;
                  if( textToImportEnd > 0 )
                  {
                      textToImportGroup = textToImport.substring( 0, textToImportEnd );
                      textToImport      = textToImport.substring( textToImportEnd + endingText.length, textToImport.length );
                  }
                  importClass.importCSVGroup( textToImportGroup );
                  startingPos = textToImport.search( "DonorID" );
              }
              else
              {
                  startingPos = 0;
              }
          }
      };
      
      return importClass;
  }]);
