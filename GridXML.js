function getDonorXML( next_donor_id )
{
   var newDonorXml = "<supporter id='" + next_donor_id.toString() + "'>";
   newDonorXml    += "<type></type>";
   newDonorXml    += "<donor_id>" + elementEncodeVal( "#donor-id" ) + "</donor_id>";
   newDonorXml    += "<name>" + elementEncodeVal( "#donor-name" )  + "</name>";
   newDonorXml    += "<relationship>" + elementEncodeVal( "#donor-relationship" ) + "</relationship>";
   newDonorXml    += "<addresses>";
   newDonorXml    += "<address primary='true'>";
   newDonorXml    += "<address1>" + elementEncodeVal( "#donor-address" )  + "</address1>";
   newDonorXml    += "<address2>" + elementEncodeVal( "#donor-address2" ) + "</address2>";
   newDonorXml    += "<city>" + elementEncodeVal( "#donor-city" ) + "</city>";
   newDonorXml    += "<state>" + elementEncodeVal( "#donor-state" ) + "</state>";
   newDonorXml    += "<zip>" + elementEncodeVal( "#donor-zip" ) + "</zip>";
   newDonorXml    += "<country>" + elementEncodeVal( "#donor-country" ) + "</country>";
   newDonorXml    += "</address>";
   newDonorXml    += "</addresses>";
   newDonorXml    += "<email_addresses>";
   newDonorXml    += "<email primary='true'>" + elementEncodeVal( "#donor-email" ) + "</email>";
   newDonorXml    += "</email_addresses>";
   newDonorXml    += "<phone_numbers>";
   newDonorXml    += "<number type='home'>" + elementEncodeVal( "#donor-homephone" ) + "</number>";
   newDonorXml    += "<number type='cell'>" + elementEncodeVal( "#donor-cellphone" ) + "</number>";
	 newDonorXml    += "</phone_numbers>";
   newDonorXml    += "</supporter>";
   return newDonorXml;
}
//------------------------------------------------------------------------------
function addNewDonorXML( dataXML, newXML )
{
    var newXMLEnd     = dataXML.search( "</supporters>" );
    var DataEnd       = dataXML.length;
    var StartXml      = dataXML.substring( 0, newXMLEnd );
    var EndXml        = dataXML.substring( newXMLEnd, DataEnd );
    
    var NewDataXML    = StartXml + newXML + EndXml;
    dataXML           = NewDataXML;
    
    return dataXML;
}
//------------------------------------------------------------------------------
function updateNewDonorXML( dataXML, dataEditID )
{
    var parsedXML = jQuery.parseXML( dataXML );
    var EditDonorNode = jQuery( parsedXML ).find( "supporter[id='" + dataEditID.toString() + "']" );
    
    jQuery( EditDonorNode ).find( "donor_id"      ).text( elementEncodeVal( "#donor-id"           ) );
    jQuery( EditDonorNode ).find( "name"          ).text( elementEncodeVal( "#donor-name"         ) );
    jQuery( EditDonorNode ).find( "relationship"  ).text( elementEncodeVal( "#donor-relationship" ) );
    
    var addressnode   = jQuery( EditDonorNode ).find( "address[primary='true']" );
    jQuery( addressnode ).find( "address1"  ).text( elementEncodeVal( "#donor-address"  ) );
    jQuery( addressnode ).find( "address2"  ).text( elementEncodeVal( "#donor-address2" ) );
    jQuery( addressnode ).find( "city"      ).text( elementEncodeVal( "#donor-city"     ) );
    jQuery( addressnode ).find( "state"     ).text( elementEncodeVal( "#donor-state"    ) );
    jQuery( addressnode ).find( "zip"       ).text( elementEncodeVal( "#donor-zip"      ) );
    jQuery( addressnode ).find( "country"   ).text( elementEncodeVal( "#donor-country"  ) );
    
    jQuery( EditDonorNode ).find( "email[primary='true']" ).text( elementEncodeVal( "#donor-email"      ) );
    jQuery( EditDonorNode ).find( "number[type='home']"   ).text( elementEncodeVal( "#donor-homephone"  ) );
    jQuery( EditDonorNode ).find( "number[type='cell']"   ).text( elementEncodeVal( "#donor-cellphone"  ) );
    
    var newDataXML = jQuery( parsedXML ).find( "app_data" ).html();
    newDataXML = "<app_data>" + newDataXML + "</app_data>";
    
    return newDataXML;
}
//------------------------------------------------------------------------------
function removeDonorXML( dataXML, donorRows )
{
    var xmltext = jQuery.parseXML( dataXML );
  
    for( i = 0; i < donorRows.length; i++ )
    {
        var DonorChecked = jQuery( xmltext ).find( "supporter[id='" + donorRows[ i ] + "']" );
        jQuery( DonorChecked ).remove();
    }
    var AfterData = jQuery( xmltext ).find( "app_data" ).html();
    dataXML = "<app_data>" + AfterData + "</app_data>"; 
    
    return dataXML;     
}
//------------------------------------------------------------------------------
function getDonationXML( next_donation_id )
{
   var newDonorXml = "<donation id='" + next_donation_id.toString() + "'>";
   newDonorXml    += "<supporter_id>" + elementEncodeVal( "#donation-donor-choice" ) + "</supporter_id>";
   newDonorXml    += "<date>" +  translateInputDate( elementEncodeVal( "#donation-date" ) ) + "</date>";
   newDonorXml    += "<amount>" + elementEncodeVal( "#donation-amount" ) + "</amount>";
   newDonorXml    += "<desc>" + elementEncodeVal( "#donation-description" ) + "</desc>";
   newDonorXml    += "</donation>";
   return newDonorXml;
}
//------------------------------------------------------------------------------
function addNewDonationXML( dataXML, newXML )
{
    var newXMLEnd     = dataXML.search( "</donations>" );
    var DataEnd       = dataXML.length;
    var StartXml      = dataXML.substring( 0, newXMLEnd );
    var EndXml        = dataXML.substring( newXMLEnd, DataEnd );
    
    var NewDataXML    = StartXml + newXML + EndXml;
    dataXML           = NewDataXML;
    
    return dataXML; 
}
//------------------------------------------------------------------------------
function updateNewDonationXML( dataXML, dataEditID )
{
    var parsedXML = jQuery.parseXML( dataXML );
    var EditDonorNode = jQuery( parsedXML ).find( "donation[id='" + dataEditID.toString() + "']" );
    
    jQuery( EditDonorNode ).find( "supporter_id"  ).text( elementEncodeVal( "#donation-donor-choice"  ) );
    jQuery( EditDonorNode ).find( "date"          ).text( elementEncodeVal( "#donation-date"          ) );
    jQuery( EditDonorNode ).find( "amount"        ).text( elementEncodeVal( "#donation-amount"        ) );
    jQuery( EditDonorNode ).find( "desc"          ).text( elementEncodeVal( "#donation-description"   ) );
    
    var newDataXML = jQuery( parsedXML ).find( "app_data" ).html();
    newDataXML = "<app_data>" + newDataXML + "</app_data>";
    
    return newDataXML;  
}
//------------------------------------------------------------------------------
function removeDonationXML( dataXML, donationRows )
{
    var xmltext = jQuery.parseXML( dataXML );
  
    for( i = 0; i < donationRows.length; i++ )
    {
        var DonationChecked = jQuery( xmltext ).find( "donation[id='" + donationRows[ i ] + "']" );
        jQuery( DonationChecked ).remove();
    }
    var AfterData = jQuery( xmltext ).find( "app_data" ).html();
    dataXML = "<app_data>" + AfterData + "</app_data>"; 
    
    return dataXML;   
}
//------------------------------------------------------------------------------