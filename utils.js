function $(q) {
  return document.querySelector(q);
}
//------------------------------------------------------------------------------
function show(q) {
  $(q).classList.remove('hide');
}
//------------------------------------------------------------------------------
function hide(q) {
  $(q).classList.add('hide');
}
//------------------------------------------------------------------------------
function validFileName(path) {
  if (!path.length) {
    error('Empty name was given.');
    return false;
  }
  if (path.indexOf('/') >= 0) {
    error('File name should not contain any slash (/): "' + path + '"');
    return false;
  }
  return true;
}
//------------------------------------------------------------------------------
function padNumber( numberText, padNumbers )
{
  while( numberText.length < padNumbers )
  {
    numberText = "0" + numberText;
  }
  return numberText;
}
//------------------------------------------------------------------------------
function log(msg) {
  document.getElementById('log').innerHTML = msg;
  //console.log(msg, arguments);
}
//------------------------------------------------------------------------------
function createElement(name, attributes) {
  var elem = document.createElement(name);
  for (var key in attributes) {
    if (key == 'id')
      elem.id = attributes[key];
    else if (key == 'innerText')
      elem.innerText = attributes[key];
    else
      elem.setAttribute(key, attributes[key]);
  }
  return elem;
}
//------------------------------------------------------------------------------
function info(msg) {
  console.log('INFO: ', arguments);
  var e = document.getElementById('info');
  e.innerText = msg;
  e.classList.remove('hide');
  window.setTimeout(function() { e.innerHTML = ''; }, 5000);
}

function getNodeValue( xmldoc, nodename )
{
  var nodefound     = jQuery( xmldoc ).find( nodename );
  var nodechildren  = jQuery( nodefound).children();
  var Result        = "";
  jQuery( nodechildren ).each( function ( index, element )
  {
    Result += jQuery( element ).text() + " ";
  });
  if( Result === "" )
  {
    Result = jQuery( nodefound ).text();
  }
  return Result;
}
//------------------------------------------------------------------------------
function InputValueEncode( inputvalue )
{
  inputvalue = inputvalue.replace( /&/g, "&amp;"   );
  inputvalue = inputvalue.replace( /'/g, "&apos;"  );
  inputvalue = inputvalue.replace( /"/g, "&quot;"  );
  inputvalue = inputvalue.replace( /</g, "&lt;"    );
  inputvalue = inputvalue.replace( />/g, "&gt;"    );
  inputvalue = inputvalue.replace( //g, ""        );
  
  return inputvalue;
}
//------------------------------------------------------------------------------
function InputValueDecode( inputvalue )
{
  inputvalue = inputvalue.replace( /&amp;/g,   "&" );
  inputvalue = inputvalue.replace( /&apos;/g,  "'" );
  inputvalue = inputvalue.replace( /&quot;/g,  '"' );
  inputvalue = inputvalue.replace( /&lt;/g,    "<" );
  inputvalue = inputvalue.replace( /&gt;/g,    ">" );
  
  return inputvalue;
}
//------------------------------------------------------------------------------
function elementEncodeVal( elementname )
{
  return InputValueEncode( jQuery( elementname ).val() );
}
//------------------------------------------------------------------------------
function elementDecodeVal( elementname, elementvalue )
{
  jQuery( elementname ).val( InputValueDecode( elementvalue ) );
}
//------------------------------------------------------------------------------
function htmlEncode(value)
{
  //create a in-memory div, set it's inner text(which jQuery automatically encodes)
  //then grab the encoded contents back out.  The div never exists on the page.
  return jQuery('<div/>').text(value).html();
}
//------------------------------------------------------------------------------
function htmlDecode(value)
{
  return jQuery('<div/>').html(value).text();
}
//------------------------------------------------------------------------------
function compareDBDates( dateOne, dateTwo )
{
  var MonthOnePos = dateOne.search( "/" );
  var MonthOneVal = parseInt( dateOne.substring( 0, MonthOnePos ) );
  dateOne         = dateOne.substring( MonthOnePos + 1, dateOne.length );
  var DayOnePos   = dateOne.search( "/" );
  var DayOneVal   = parseInt( dateOne.substring( 0, DayOnePos ) );
  var YearOneVal  = parseInt( dateOne.substring( DayOnePos + 1, dateOne.length ) );
  
  var MonthTwoPos = dateTwo.search( "/" );
  var MonthTwoVal = parseInt( dateTwo.substring( 0, MonthTwoPos ) );
  dateTwo         = dateTwo.substring( MonthTwoPos + 1, dateTwo.length );
  var DayTwoPos   = dateTwo.search( "/" );
  var DayTwoVal   = parseInt( dateTwo.substring( 0, DayTwoPos ) );
  var YearTwoVal  = parseInt( dateTwo.substring( DayTwoPos + 1, dateTwo.length ) ); 
  
  if( YearOneVal > YearTwoVal )
  {
      return 1; 
  }
  else if( YearOneVal < YearTwoVal )
  {
      return -1;
  }
  else
  {
    if( MonthOneVal > MonthTwoVal )
    {
        return 1;
    }
    else if( MonthOneVal < MonthTwoVal )
    {
        return -1;
    }
    else
    {
        if( DayOneVal > DayTwoVal )
        {
            return 1;
        }
        else if( DayOneVal < DayTwoVal )
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
  }
  return 0;
}
//------------------------------------------------------------------------------
function translateDBDate( dateText )
{
  if( dateText.search( "-") > 0 )
  {
    return dateText;
  }
  var MonthPos = dateText.search( "/" );
  var MonthVal = dateText.substring( 0, MonthPos );
  dateText     = dateText.substring( MonthPos + 1, dateText.length );
  var DayPos   = dateText.search( "/" );
  var DayVal   = dateText.substring( 0, DayPos );
  var YearVal  = dateText.substring( DayPos + 1, dateText.length ); 
  
  return YearVal + "-" + padNumber( MonthVal, 2 ) + "-" + padNumber( DayVal, 2 );
}
//------------------------------------------------------------------------------
function translateInputDate( dateText )
{
  if( dateText.search( "/") > 0 )
  {
    return dateText;
  }
  var YearPos  = dateText.search( "-" );
  var YearVal  = dateText.substring( 0, YearPos );
  dateText     = dateText.substring( YearPos + 1, dateText.length );
  var MonthPos = dateText.search( "-" );
  var MonthVal = dateText.substring( 0, MonthPos );
  var DayVal  = dateText.substring( MonthPos + 1, dateText.length ); 
  
  return MonthVal + "/" + padNumber( DayVal, 2 ) + "/" + padNumber( YearVal, 2 );
}
//------------------------------------------------------------------------------
function getInputDateValue( dateText )
{
  var YearPos = 0;
  var MonthPos = 0;
  var DayPos = 0;
  
  var YearVal = 0;
  var MonthVal = 0;
  var DayVal = 0;
  if( dateText.search( "/") > 0 )
  {
      MonthPos = dateText.search( "/" );
      MonthVal = dateText.substring( 0, MonthPos );
      dateText     = dateText.substring( MonthPos + 1, dateText.length );
      DayPos   = dateText.search( "/" );
      DayVal   = dateText.substring( 0, DayPos );
      YearVal  = dateText.substring( DayPos + 1, dateText.length ); 
  
      return new Date( YearVal, padNumber( MonthVal, 2 ), padNumber( DayVal, 2 ) );
  }
  else
  {
      YearPos  = dateText.search( "-" );
      YearVal  = dateText.substring( 0, YearPos );
      dateText     = dateText.substring( YearPos + 1, dateText.length );
      MonthPos = dateText.search( "-" );
      MonthVal = dateText.substring( 0, MonthPos );
      DayVal  = dateText.substring( MonthPos + 1, dateText.length ); 
      return new Date( YearVal, padNumber( MonthVal, 2 ), padNumber( DayVal, 2 ) );
  }
}
//------------------------------------------------------------------------------
function getInputDateLabels( dateOne, dateTwo, labelArray )
{
    var YearOneVal  = dateOne.getFullYear();
    var MonthOneVal = dateOne.getMonth();
  
    var YearTwoVal  = dateTwo.getFullYear();
    var MonthTwoVal = dateTwo.getMonth();
  
    while( ( MonthOneVal <= MonthTwoVal ) || ( YearOneVal < YearTwoVal ) )
    {
        labelArray.push( padNumber( MonthOneVal.toString(), 2 ) + "/" + YearOneVal.toString() );
        MonthOneVal++;
        if( MonthOneVal > 12 )
        {
            YearOneVal++;
            MonthOneVal = 1;
        }
    }
}
//------------------------------------------------------------------------------
function getInputDateLabelFormat( dateText )
{
    var MonthPos = dateText.search( "/" );
    var MonthVal = dateText.substring( 0, MonthPos );
    dateText     = dateText.substring( MonthPos + 1, dateText.length );
    var DayPos   = dateText.search( "/" );
    var DayVal   = dateText.substring( 0, DayPos );
    var YearVal  = dateText.substring( DayPos + 1, dateText.length ); 
    
    return MonthVal + "/" + YearVal;
}
//------------------------------------------------------------------------------
function getInputDateFormat( dateValue )
{
  var DayVal    = dateValue.getDay();
  var MonthVal  = dateValue.getMonth() + 1;
  var YearVal   = dateValue.getFullYear();
  
  var Result = YearVal.toString() + "-" + padNumber( MonthVal.toString(), 2 ) + "-" + padNumber( DayVal.toString(), 2 );
  
  return Result;
}
//------------------------------------------------------------------------------
//Month is 1 based
function daysInMonth(month,year) 
{
    return new Date(year, month, 0).getDate();
}
//------------------------------------------------------------------------------
function getDateDifference( dateValue, DayDiff, MonthDiff, YearDiff )
{
  var DayVal    = parseInt( dateValue.getDay() ) + DayDiff;
  var MonthVal  = parseInt( dateValue.getMonth() ) + MonthDiff + 1;
  var YearVal   = parseInt( dateValue.getFullYear() ) + YearDiff;
  
  if( DayVal <= 0 )
  {
    MonthVal  = MonthVal - 1;
    if( MonthVal > 0 )
    {
      DayVal = DayVal + daysInMonth( MonthVal, YearVal );
    }
  }
  if( MonthVal <= 0 )
  {
    MonthVal = MonthVal + 12;
    YearVal = YearVal - 1;
    var daysInThisMonth = daysInMonth( MonthVal, YearVal );
    if( DayVal <= 0 )
    {
      DayVal = DayVal + daysInThisMonth;
    }
    else if( DayVal > daysInThisMonth )
    {
      DayVal = DaysInThisMonth;
    }
  }
  var Result = new Date(YearVal.toString() + "-" + padNumber( MonthVal.toString(), 2 ) + "-" + padNumber( DayVal.toString(), 2 ));
  
  return Result;
}
//------------------------------------------------------------------------------
function getInputDateFormatDifference( dateValue, DayDiff, MonthDiff, YearDiff )
{
  var ResultDateVal = getDateDifference( dateValue, DayDiff, MonthDiff, YearDiff );
  var DayVal    = parseInt( ResultDateVal.getDay() );
  var MonthVal  = parseInt( ResultDateVal.getMonth() );
  var YearVal   = parseInt( ResultDateVal.getFullYear() );
  
  var Result = YearVal.toString() + "-" + padNumber( MonthVal.toString(), 2 ) + "-" + padNumber( DayVal.toString(), 2 );
  
  return Result;
}
//------------------------------------------------------------------------------
function error(msg) {
  log( msg );
  console.log('ERROR: ', arguments);
  var message = '';
  for (var i = 0; i < arguments.length; i++) {
    var description = '';
    if (arguments[i] instanceof FileError) {
      switch (arguments[i].code) {
        case FileError.QUOTA_EXCEEDED_ERR:
          description = 'QUOTA_EXCEEDED_ERR';
          break;
        case FileError.NOT_FOUND_ERR:
          description = 'NOT_FOUND_ERR';
          break;
        case FileError.SECURITY_ERR:
          description = 'SECURITY_ERR';
          break;
        case FileError.INVALID_MODIFICATION_ERR:
          description = 'INVALID_MODIFICATION_ERR';
          break;
        case FileError.INVALID_STATE_ERR:
          description = 'INVALID_STATE_ERR';
          break;
        default:
          description = 'Unknown Error';
          break;
      }
      message += ': ' + description;
    } else if (arguments[i].fullPath) {
      message += arguments[i].fullPath + ' ';
    } else {
      message += arguments[i] + ' ';
    }
  }
  var e = document.getElementById('error');
  e.innerText = 'ERROR:' + message;
  e.classList.remove('hide');
  window.setTimeout(function() { e.innerHTML = ''; }, 5000);
}